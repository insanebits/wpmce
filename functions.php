<?php

/**
 * Prints the styles that were queued too late for the HTML head.
 *
 * @since 3.3.0
 */
function print_late_styles() {
    global $wp_styles, $concatenate_scripts;

    if ( !is_a($wp_styles, 'WP_Styles') )
        return;

    $wp_styles->do_concat = $concatenate_scripts;
    $wp_styles->do_footer_items();

    /**
     * Filter whether to print the styles queued too late for the HTML head.
     *
     * @since 3.3.0
     *
     * @param bool $print Whether to print the 'late' styles. Default true.
     */
    if ( apply_filters( 'print_late_styles', true ) ) {
        //    _print_styles();
    }

    $wp_styles->reset();
    return $wp_styles->done;
}


/**
 * Prints the scripts that were queued for the footer or too late for the HTML head.
 *
 * @since 2.8.0
 */
function print_footer_scripts() {
    global $wp_scripts, $concatenate_scripts;

    if ( !is_a($wp_scripts, 'WP_Scripts') )
        return array(); // No need to run if not instantiated.

    script_concat_settings();
    $wp_scripts->do_concat = $concatenate_scripts;
    $wp_scripts->do_footer_items();

    /**
     * Filter whether to print the footer scripts.
     *
     * @since 2.8.0
     *
     * @param bool $print Whether to print the footer scripts. Default true.
     */
    if ( apply_filters( 'print_footer_scripts', true ) ) {
        _print_scripts();
    }

    $wp_scripts->reset();
    return $wp_scripts->done;
}

/**
 * @internal use
 */
function _print_scripts() {
    global $wp_scripts, $compress_scripts;

    $zip = $compress_scripts ? 1 : 0;
    if ( $zip && defined('ENFORCE_GZIP') && ENFORCE_GZIP )
        $zip = 'gzip';

    if ( $concat = trim( $wp_scripts->concat, ', ' ) ) {

        if ( !empty($wp_scripts->print_code) ) {
            echo "\n<script type='text/javascript'>\n";
            echo "/* <![CDATA[ */\n"; // not needed in HTML 5
            echo $wp_scripts->print_code;
            echo "/* ]]> */\n";
            echo "</script>\n";
        }

        $concat = str_split( $concat, 128 );
        $concat = 'load%5B%5D=' . implode( '&load%5B%5D=', $concat );

        $src = $wp_scripts->base_url . "/wp-admin/load-scripts.php?c={$zip}&" . $concat . '&ver=' . $wp_scripts->default_version;
        echo "<script type='text/javascript' src='" . esc_attr($src) . "'></script>\n";
    }

    if ( !empty($wp_scripts->print_html) )
        echo $wp_scripts->print_html;
}

function _wp_footer_scripts() {
    print_late_styles();
    print_footer_scripts();
}

function __($text) { return $text;}

function _x($text) { return $text;}

function is_rtl() { return false; }

function wp_print_footer_scripts() {
    /**
     * Fires when footer scripts are printed.
     *
     * @since 2.8.0
     */
    do_action( 'wp_print_footer_scripts' );
}

function script_concat_settings() {
    global $concatenate_scripts, $compress_scripts, $compress_css;

    $compressed_output = ( ini_get('zlib.output_compression') || 'ob_gzhandler' == ini_get('output_handler') );

    if ( ! isset($concatenate_scripts) ) {
        $concatenate_scripts = defined('CONCATENATE_SCRIPTS') ? CONCATENATE_SCRIPTS : true;
        if ( ! is_admin() || ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ) )
            $concatenate_scripts = false;
    }

    if ( ! isset($compress_scripts) ) {
        $compress_scripts = defined('COMPRESS_SCRIPTS') ? COMPRESS_SCRIPTS : true;
        if ( $compress_scripts && ( ! get_site_option('can_compress_scripts') || $compressed_output ) )
            $compress_scripts = false;
    }

    if ( ! isset($compress_css) ) {
        $compress_css = defined('COMPRESS_CSS') ? COMPRESS_CSS : true;
        if ( $compress_css && ( ! get_site_option('can_compress_scripts') || $compressed_output ) )
            $compress_css = false;
    }
}

function add_thickbox() {
    wp_enqueue_script( 'thickbox' );
    wp_enqueue_style( 'thickbox' );

    if ( is_network_admin() )
        add_action( 'admin_head', '_thickbox_path_admin_subfolder' );
}

function get_editor_stylesheets() {
    $stylesheets = array();
    // load editor_style.css if the current theme supports it
    if ( ! empty( $GLOBALS['editor_styles'] ) && is_array( $GLOBALS['editor_styles'] ) ) {
        $editor_styles = $GLOBALS['editor_styles'];

        $editor_styles = array_unique( array_filter( $editor_styles ) );
        $style_uri = get_stylesheet_directory_uri();
        $style_dir = get_stylesheet_directory();

        // Support externally referenced styles (like, say, fonts).
        foreach ( $editor_styles as $key => $file ) {
            if ( preg_match( '~^(https?:)?//~', $file ) ) {
                $stylesheets[] = esc_url_raw( $file );
                unset( $editor_styles[ $key ] );
            }
        }

        // Look in a parent theme first, that way child theme CSS overrides.
        if ( is_child_theme() ) {
            $template_uri = get_template_directory_uri();
            $template_dir = get_template_directory();

            foreach ( $editor_styles as $key => $file ) {
                if ( $file && file_exists( "$template_dir/$file" ) ) {
                    $stylesheets[] = "$template_uri/$file";
                }
            }
        }

        foreach ( $editor_styles as $file ) {
            if ( $file && file_exists( "$style_dir/$file" ) ) {
                $stylesheets[] = "$style_uri/$file";
            }
        }
    }
    return $stylesheets;
}

function current_theme_supports( $feature ) {
    global $_wp_theme_features;

    if ( 'custom-header-uploads' == $feature )
        return current_theme_supports( 'custom-header', 'uploads' );

    if ( !isset( $_wp_theme_features[$feature] ) )
        return false;

    if ( 'title-tag' == $feature ) {
        // Don't confirm support unless called internally.
        $trace = debug_backtrace();
        if ( ! in_array( $trace[1]['function'], array( '_wp_render_title_tag', 'wp_title' ) ) ) {
            return false;
        }
    }

    // If no args passed then no extra checks need be performed
    if ( func_num_args() <= 1 )
        return true;

    $args = array_slice( func_get_args(), 1 );

    switch ( $feature ) {
        case 'post-thumbnails':
            // post-thumbnails can be registered for only certain content/post types by passing
            // an array of types to add_theme_support(). If no array was passed, then
            // any type is accepted
            if ( true === $_wp_theme_features[$feature] )  // Registered for all types
                return true;
            $content_type = $args[0];
            return in_array( $content_type, $_wp_theme_features[$feature][0] );

        case 'html5':
        case 'post-formats':
            // specific post formats can be registered by passing an array of types to
            // add_theme_support()

            // Specific areas of HTML5 support *must* be passed via an array to add_theme_support()

            $type = $args[0];
            return in_array( $type, $_wp_theme_features[$feature][0] );

        case 'custom-header':
        case 'custom-background' :
            // specific custom header and background capabilities can be registered by passing
            // an array to add_theme_support()
            $header_support = $args[0];
            return ( isset( $_wp_theme_features[$feature][0][$header_support] ) && $_wp_theme_features[$feature][0][$header_support] );
    }

    /**
     * Filter whether the current theme supports a specific feature.
     *
     * The dynamic portion of the hook name, `$feature`, refers to the specific theme
     * feature. Possible values include 'post-formats', 'post-thumbnails', 'custom-background',
     * 'custom-header', 'menus', 'automatic-feed-links', and 'html5'.
     *
     * @since 3.4.0
     *
     * @param bool   true     Whether the current theme supports the given feature. Default true.
     * @param array  $args    Array of arguments for the feature.
     * @param string $feature The theme feature.
     */
    return apply_filters( "current_theme_supports-{$feature}", true, $args, $_wp_theme_features[$feature] );
}

function get_locale() {
    global $locale, $wp_local_package;

    if ( isset( $locale ) ) {
        /**
         * Filter WordPress install's locale ID.
         *
         * @since 1.5.0
         *
         * @param string $locale The locale ID.
         */
        return apply_filters( 'locale', $locale );
    }

    if ( isset( $wp_local_package ) ) {
        $locale = $wp_local_package;
    }

    // WPLANG was defined in wp-config.
    if ( defined( 'WPLANG' ) ) {
        $locale = WPLANG;
    }


    $db_locale = get_option( 'WPLANG' );
    if ( $db_locale !== false ) {
        $locale = $db_locale;
    }


    if ( empty( $locale ) ) {
        $locale = 'en_US';
    }

    /** This filter is documented in wp-includes/l10n.php */
    return apply_filters( 'locale', $locale );
}

function wp_editor( $content, $editor_id, $settings = array() ) {
    _WP_Editors::editor($content, $editor_id, $settings);
}

function wp_enqueue_scripts() {
    /**
     * Fires when scripts and styles are enqueued.
     *
     * @since 2.8.0
     */
    do_action( 'wp_enqueue_scripts' );
}

function wp_enqueue_styles() {
    /**
     * Fires when scripts and styles are enqueued.
     *
     * @since 2.8.0
     */
    do_action( 'wp_enqueue_styles' );
}

function editor_helper_scripts()
{
    wp_register_script('utils', site_url(INC_DIR . '/js/utils.js'), array(), '1.0.0', true);
    wp_enqueue_script('utils');

    wp_enqueue_script('editor-helper', site_url(INC_DIR . '/js/editor.js'), array(), false, true);
}

function editor_helper_styles()
{
    wp_enqueue_style('editor', site_url(INC_DIR . '/css/editor.css'));
    wp_enqueue_style('dashicons', site_url(INC_DIR . '/css/dashicons.css'));
}

add_filter('wp_enqueue_styles', 'editor_helper_styles');
add_filter('wp_enqueue_scripts', 'editor_helper_scripts');
