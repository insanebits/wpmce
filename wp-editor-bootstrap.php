<?php

define('INC_DIR', 'wp-core');

// replaces for wp-load.php

define( 'ABSPATH', dirname(__FILE__) . '/' );

error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );

// replaces for wp-includes/option.php file

function get_option($option, $default = false)
{
    // editor basically needs only three options so so get rid of whole lot of useless database tables
    $config = array(
        'baseurl' => 'http://' . $_SERVER['SERVER_NAME'],
        'siteurl' => 'http://' . $_SERVER['SERVER_NAME'],
        'WPLANG' => ''
    );

    if(isset($config[$option])) {
        return $config[$option];
    }

    return $default;
}

function get_site_option($option, $default = false)
{
    return get_option($option, $default);
}

// replaces for wp-includes/default-constants.php

function wp_initial_constants()
{
    global $blog_id;

    // set memory limits
    if(!defined('WP_MEMORY_LIMIT')) {
        if(is_multisite()) {
            define('WP_MEMORY_LIMIT', '64M');
        } else {
            define('WP_MEMORY_LIMIT', '40M');
        }
    }

    if(!defined('WP_MAX_MEMORY_LIMIT')) {
        define('WP_MAX_MEMORY_LIMIT', '256M');
    }

    /**
     * The $blog_id global, which you can change in the config allows you to create a simple
     * multiple blog installation using just one WordPress and changing $blog_id around.
     *
     * @global int $blog_id
     * @since 2.0.0
     */
    if(!isset($blog_id))
        $blog_id = 1;

    // set memory limits.
    if(function_exists('memory_get_usage')) {
        $current_limit = @ini_get('memory_limit');
        $current_limit_int = intval($current_limit);
        if(false !== strpos($current_limit, 'G'))
            $current_limit_int *= 1024;
        $wp_limit_int = intval(WP_MEMORY_LIMIT);
        if(false !== strpos(WP_MEMORY_LIMIT, 'G'))
            $wp_limit_int *= 1024;

        if(-1 != $current_limit && (-1 == WP_MEMORY_LIMIT || $current_limit_int < $wp_limit_int))
            @ini_set('memory_limit', WP_MEMORY_LIMIT);
    }

    if(!defined('WP_CONTENT_DIR'))
        define('WP_CONTENT_DIR', ABSPATH . 'wp-content'); // no trailing slash, full paths only - WP_CONTENT_URL is defined further down

    // Add define('WP_DEBUG', true); to wp-config.php to enable display of notices during development.
    if(!defined('WP_DEBUG'))
        define('WP_DEBUG', false);

    // Add define('WP_DEBUG_DISPLAY', null); to wp-config.php use the globally configured setting for
    // display_errors and not force errors to be displayed. Use false to force display_errors off.
    if(!defined('WP_DEBUG_DISPLAY'))
        define('WP_DEBUG_DISPLAY', true);

    // Add define('WP_DEBUG_LOG', true); to enable error logging to wp-content/debug.log.
    if(!defined('WP_DEBUG_LOG'))
        define('WP_DEBUG_LOG', false);

    if(!defined('WP_CACHE'))
        define('WP_CACHE', false);

    /**
     * Private
     */
    if(!defined('MEDIA_TRASH'))
        define('MEDIA_TRASH', false);

    if(!defined('SHORTINIT'))
        define('SHORTINIT', false);

    // Constants for expressing human-readable intervals
    // in their respective number of seconds.
    define('MINUTE_IN_SECONDS', 60);
    define('HOUR_IN_SECONDS', 60 * MINUTE_IN_SECONDS);
    define('DAY_IN_SECONDS', 24 * HOUR_IN_SECONDS);
    define('WEEK_IN_SECONDS', 7 * DAY_IN_SECONDS);
    define('YEAR_IN_SECONDS', 365 * DAY_IN_SECONDS);
}

// replaces for wp-includes/load.php

if(!function_exists('is_admin')) {
    /**
     * Not really sure why it doesn't work when return true
     * @return bool
     */
    function is_admin()
    {
        return false;
    }
}

// replaces for wp-settings.php

define('WPINC', 'wp-includes');
define('MULTISITE', false);

// Load early WordPress files.
require(ABSPATH . INC_DIR . '/compat.php');
require(ABSPATH . INC_DIR . '/functions.php');
require(ABSPATH . INC_DIR . '/class-wp.php');
require(ABSPATH . INC_DIR . '/class-wp-error.php');
require(ABSPATH . INC_DIR . '/plugin.php');
require(ABSPATH . INC_DIR . '/pomo/mo.php');


// replaces for wp-includes/default-filters.php

// this goes after wp-load since it requires plugin.php to work
add_action('wp_head', 'wp_enqueue_scripts', 1);
add_action('wp_head', 'wp_enqueue_styles', 1);

require_once ABSPATH . INC_DIR . "/formatting.php";
require_once ABSPATH . INC_DIR . '/class-wp-editor.php';
require_once ABSPATH . INC_DIR . "/link-template.php";
require_once ABSPATH . INC_DIR . "/class.wp-dependencies.php";
require_once ABSPATH . INC_DIR . "/class.wp-styles.php";
require_once ABSPATH . INC_DIR . "/class.wp-scripts.php";
require_once ABSPATH . INC_DIR . "/functions.wp-scripts.php";
require_once ABSPATH . INC_DIR . "/functions.wp-styles.php";

require_once ABSPATH . 'functions.php';

do_action('wp_head');



