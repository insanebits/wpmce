<?php
define('SHORTINIT', true);

require_once "wp-editor-bootstrap.php";

?>
<html>
<head>
    <?php wp_print_scripts(); ?>
    <?php wp_print_styles(); ?>
</head>
<body>
<div class="">
    <?= wp_editor('this first editor', 'editor1'); ?>
</div>

<div class="">
    <?= wp_editor('this is second editor', 'editor2'); ?>
</div>

<div class="">
    <?= wp_editor('this is third editor', 'editor3'); ?>
</div>

<?php wp_print_footer_scripts(); ?>

</body>
</html>